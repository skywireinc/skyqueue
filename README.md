# SkyQueue

A lightweight simple messaging queue for desktop and mobile platforms that utilizes Akavache for its data store. See [Akavache](https://github.com/reactiveui/Akavache) for more information on the data storage of the queue.

## Table of contents
[TOC]

## Requirements

Supported platforms

* .NET Standard 2.0
* .NET 4.6.1
* .NET 4.6.2

## Implemented functionality

* Push messages to a cached queue
* Process messages one at a time in order of being enqueued
* Retry failed messages
* Purge or Resync failed messages in a dead letter queue.

## Referencing

SkyQueue is available as a nuget package from the package manager console:

```csharp
Install-Package SkyQueue
```

## Usage

### Enqueue

Pushes a new message into the main queue.

```csharp
public async Task MyTask()
{
	var messageQueue = SkyQueue
		.Create("application")
		.UseInMemoryStorage()
		.Build();

	await messageQueue.EnqueueAsync("message");
}
```

### Subscribe

Will perform an action for each message that is pushed to the queue.

```csharp
public async Task MyTask()
{
	SkyQueue
		.Create("application")
		.UseInMemoryStorage()
		.Subscribe(m =>
		{
			Console.WriteLine($"Message received: {m}");

			// Do work

			return Task.CompletedTask;
		})
		.Build();
}
```

The message queue is built via a fluent builder where you will configure the settings needed to push and process messages. 

### Setup

Create specifies the location akavache will use to store the sqlite data store.

_Recommended to use the application name._

```csharp
SkyQueue
	.Create("application");
```

Storage location is required when building the queue. See [Akavache](https://github.com/reactiveui/Akavache) for more information on each location.

```csharp
SkyQueue
	.Create("application")
	.UsingInMemoryStorage();
```

```csharp
SkyQueue
	.Create("application")
	.UsingLocalStorage();
```

```csharp
SkyQueue
	.Create("application")
	.UsingSecureStorage();
```

```csharp
SkyQueue
	.Create("application")
	.UsingUserAccountStorage();
```

SkyQueue uses a timer mechanism to check for new messages. Setting the interval will set how often the queue should be rechecked for new messages. 

_This is optional and will default to 5000 milliseconds._

```csharp
SkyQueue
	.Create("application")
	.UsingInMemoryStorage()
	.PollFor(1000);
```
	
Sets the command to be executed when a new message is dequeued. If an exception is thrown the message will be pushed to the retry queue if configured and then into the dead letter queue if configured. Any Subsribe running will process whats in the queue with no knowledge of any other threads or applications also subscribing, it is recommended that only one Subscribe is being used.

_This is required if messages need to be processed._

```csharp
SkyQueue
	.Create("application")
	.UsingInMemoryStorage()
	.Subscribe(m => Console.WriteLine(m));
```

Configures a retry queue when the command being executed fails. The message will be pushed to the retry queue for the interval specified. Afterwards the message will be requeued into the main to be processed. When the number of retries has exceeded the specified attempts the message will end up in the dead letter queue if configured.

_This is optional if no retry is needed._

```csharp
SkyQueue
	.Create("application")
	.UsingInMemoryStorage()
	.Subscribe(m => Console.WriteLine(m))
	.WithRetry(1, 30000);
```

Configures a dead letter queue where all failed retried messages end up.

_This is optional if storage for dead messages is not needed._Recommended

```csharp
SkyQueue
	.Create("application")
	.UsingInMemoryStorage()
	.Subscribe(m => Console.WriteLine(m))
	.WithDeadLetter();
```

Transfers all dead messages in the dead letter queue back into the main queue.

```csharp
var messageQueue = SkyQueue
	.Create("application")
	.UseInMemoryStorage()
	.Build();

await messageQueue.ResyncAsync();
```

Removes all dead messages from the dead letter queue.

```csharp
var messageQueue = SkyQueue
	.Create("application")
	.UseInMemoryStorage()
	.Build();

await messageQueue.PurgeDeadLetterAsync();
```

## Example

Sample projects is included in the repository where there are two apps to push a message and one that subscribes to each message. Run both `Pusblisher` and `Subscriber` projects and watch messages flow through the queues. Inputting `error` as the message will force the message into the retry queue.

Another sample project `Features` includes `Resync` and `Purge` functionality.

## Development

### Installation

* [.NET CORE 2.2](https://dotnet.microsoft.com/download/thank-you/dotnet-sdk-2.2.401-windows-x64-installer)
* .NET STANDARD 2.0

## License

Copyright (c) 2019 SkyWire

SkyQueue is provided as-is under the MIT license. For more information see [LICENSE](https://opensource.org/licenses/MIT).
