﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SkyWire.SkyQueue.SampleApp.Subscriber
{
    public class Program
    {
        static void Main(string[] args)
        {

            var cancellationTokenSource = new CancellationTokenSource();
            Console.CancelKeyPress += (_, __) => cancellationTokenSource.Cancel();

            try
            {
                var task = Task.Run(() =>
                {
                    var messageQueue = SkyQueueBuilder
                        .Create("skyqueue")
                        .UsingLocalStorage()
                        .Subscribe(m =>
                        {
                            Console.WriteLine($"Message received: {m}");

                            if (m == "error")
                                throw new Exception(m);

                            return Task.CompletedTask;
                        })
                        .WithRetry(1, 5000)
                        .WithDeadLetter()
                        .Build();

                    Console.WriteLine("Listening for messages...");

                    while (true) { }
                });

                Task.WaitAll(new Task[] { task }, cancellationTokenSource.Token);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}
