﻿using SkyWire.SkyQueue.Services;
using System;
using System.Threading.Tasks;

namespace SkyWire.SkyQueue.Builders
{
    /// <summary>
    /// Represents a fluent builder to create <see cref="IMessageQueue"/>.
    /// </summary>
    public interface IMessageQueueBuilder
    {
        /// <summary>
        /// Sets the location of where the queue should live.
        /// </summary>
        /// <param name="name">The location of the data store.</param>
        /// <returns><see cref="IMessageQueueBuilderStorage"/>.</returns>
        IMessageQueueBuilderStorage Create(string name);
    }

    /// <summary>
    /// Represents a fluent builder to create <see cref="IMessageQueue"/>.
    /// </summary>
    public interface IMessageQueueBuilderStorage
    {
        /// <summary>
        /// Sets the queue to use a cached data location.
        /// </summary>
        /// <returns><see cref="IMessageQueueBuilderSubscribe"/>.</returns>
        IMessageQueueBuilderSubscribe UsingLocalStorage();

        /// <summary>
        /// Sets the queue to use a secured location.
        /// </summary>
        /// <returns><see cref="IMessageQueueBuilderSubscribe"/>.</returns>
        IMessageQueueBuilderSubscribe UsingSecureStorage();

        /// <summary>
        /// Sets the queue to use in memory location.
        /// </summary>
        /// <returns><see cref="IMessageQueueBuilderSubscribe"/>.</returns>
        IMessageQueueBuilderSubscribe UsingInMemoryStorage();

        /// <summary>
        /// Sets the queue to use user settings location.
        /// </summary>
        /// <returns><see cref="IMessageQueueBuilderSubscribe"/>.</returns>
        IMessageQueueBuilderSubscribe UsingUserAccountStorage();
    }

    /// <summary>
    /// Represents a fluent builder to create <see cref="IMessageQueue"/>.
    /// </summary>
    public interface IMessageQueueBuilderSubscribe
    {
        /// <summary>
        /// Sets when each queue should be processed.
        /// </summary>
        /// <param name="interval">How often the queues should be processed.</param>
        /// <returns></returns>
        IMessageQueueBuilderSubscribe PollFor(int interval);

        /// <summary>
        /// Sets the command to execute when there is a new message in the queue.
        /// </summary>
        /// <param name="command">What should execute.</param>
        /// <returns><see cref="IMessageQueueBuilderSettings"/>.</returns>
        IMessageQueueBuilderSettings Subscribe(Func<string, Task> command);

        /// <summary>
        /// Creates a <see cref="IMessageQueue"/>.
        /// </summary>
        /// <returns>Created <see cref="IMessageQueue"/>.</returns>
        IMessageQueue Build();
    }

    /// <summary>
    /// Represents a fluent builder to create <see cref="IMessageQueue"/>.
    /// </summary>
    public interface IMessageQueueBuilderSettings
    {
        /// <summary>
        /// Determines how many retry attempts should be made and how long it should wait until retrying again.
        /// </summary>
        /// <param name="retryAttempts">How many times a message should be retried.</param>
        /// <param name="interval">How long to wait until a message is retried.</param>
        /// <returns><see cref="IMessageQueueBuilderSettings"./></returns>
        IMessageQueueBuilderSettings WithRetry(int retryAttempts, long interval);

        /// <summary>
        /// Determines whether a failed message will be stored in the dead letter queue rather than being deleted.
        /// </summary>
        /// <returns><see cref="IMessageQueueBuilderSettings"./></returns>
        IMessageQueueBuilderSettings WithDeadLetter();

        /// <summary>
        /// Creates a valid <see cref="IMessageQueue"/>.
        /// </summary>
        /// <returns><see cref="IMessageQueue"/>.</returns>
        IMessageQueue Build();
    }
}
