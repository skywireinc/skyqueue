﻿using System;
using System.Threading.Tasks;
using SkyWire.SkyQueue.DataAccess;
using SkyWire.SkyQueue.Services;

namespace SkyWire.SkyQueue.Builders
{
    internal class MessageQueueBuilder :
        IMessageQueueBuilder,
        IMessageQueueBuilderSubscribe,
        IMessageQueueBuilderSettings,
        IMessageQueueBuilderStorage
    {
        private string _applicationName;
        private StorageType _storageType;
        private Func<string, Task> _command;
        private bool _enableDeadLetter;
        private int _retryAttempts;
        private long _retryInterval;
        private int? _pollInterval;

        public IMessageQueue Build()
        {
            var messageQueue = new AkavacheMessageQueue(
                new AkavacheQueue(_applicationName, _storageType))
            {
                DeadLetterEnabled = _enableDeadLetter,
                RetryAttempts = _retryAttempts,
                RetryInterval = _retryInterval,
                Command = _command,
                PollInterval = _pollInterval ?? 5000,
            };

            if(_command != null)
                messageQueue.Start();

            _applicationName = string.Empty;
            _storageType = StorageType.None;
            _command = null;
            _enableDeadLetter = false;
            _retryAttempts = 0;
            _retryInterval = 0;
            _pollInterval = null;

            return messageQueue;
        }

        public IMessageQueueBuilderStorage Create(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new NullReferenceException(nameof(name));

            _applicationName = name;

            return this;
        }

        public IMessageQueueBuilderSettings Subscribe(Func<string, Task> command)
        {
            _command = command;

            return this;
        }

        public IMessageQueueBuilderSubscribe UsingInMemoryStorage()
        {
            _storageType = StorageType.InMemory;

            return this;
        }

        public IMessageQueueBuilderSubscribe UsingLocalStorage()
        {
            _storageType = StorageType.Local;

            return this;
        }

        public IMessageQueueBuilderSubscribe UsingSecureStorage()
        {
            _storageType = StorageType.Secure;

            return this;
        }

        public IMessageQueueBuilderSubscribe UsingUserAccountStorage()
        {
            _storageType = StorageType.UserAccount;

            return this;
        }

        public IMessageQueueBuilderSubscribe PollFor(int interval)
        {
            if (interval < 0)
                throw new ArgumentOutOfRangeException(nameof(interval));

            _pollInterval = interval;

            return this;
        }

        public IMessageQueueBuilderSettings WithDeadLetter()
        {
            _enableDeadLetter = true;

            return this;
        }

        public IMessageQueueBuilderSettings WithRetry(int retryAttempts, long interval)
        {
            if (retryAttempts < 0)
                throw new ArgumentOutOfRangeException(nameof(retryAttempts));

            if (interval < 0)
                throw new ArgumentOutOfRangeException(nameof(interval));

            _retryAttempts = retryAttempts;
            _retryInterval = interval;

            return this;
        }
    }
}
