﻿using SkyWire.SkyQueue.Models;
using System.Threading.Tasks;

namespace SkyWire.SkyQueue.Services
{
    /// <summary>
    /// Represents the service to enqueue messages into a queue to be processed.
    /// </summary>
    public interface IMessageQueue
    {
        /// <summary>
        /// Push a message into the queue.
        /// </summary>
        /// <param name="message">The message to be processed.</param>
        /// <returns><see cref="Task"/>.</returns>
        Task EnqueueAsync(string message);

        /// <summary>
        /// Removes all message from the dead letter queue and repushes it to the main queue as a new <see cref="Payload"/>.
        /// </summary>
        /// <param name="deadLetter"></param>
        /// <returns></returns>
        Task RequeueAsync();

        /// <summary>
        /// Removes all messages from the dead letter queue.
        /// </summary>
        /// <param name="deadLetter"></param>
        /// <returns></returns>
        Task PurgeDeadLetterAsync();

        /// <summary>
        /// Gets the number of messages in the main queue.
        /// </summary>
        /// <returns>The mains queue message count.</returns>
        Task<long> GetCountAsync();

        /// <summary>
        /// Gets the number of messages in the retry queue.
        /// </summary>
        /// <returns>The retry queue message count.</returns>
        Task<long> GetRetryCountAsync();

        /// <summary>
        /// Gets the number of messages in the dead letter queue.
        /// </summary>
        /// <returns>The dead letter queue message count.</returns>
        Task<long> GetDeadLetterCountAsync();
    }
}
