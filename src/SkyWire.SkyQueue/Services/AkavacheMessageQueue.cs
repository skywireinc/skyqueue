﻿using System;
using System.Threading.Tasks;
using log4net;
using SkyWire.SkyQueue.Common;
using SkyWire.SkyQueue.DataAccess;
using SkyWire.SkyQueue.Models;
using Timer = System.Timers.Timer;

namespace SkyWire.SkyQueue.Services
{
    /// <summary>
    /// Represents akavache queue processing.
    /// </summary>
    internal class AkavacheMessageQueue : IMessageQueue
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly AkavacheQueue _akavacheCache;
        private Timer _payloadTimer;
        private Timer _retryTimer;

        /// <summary>
        /// <see cref="AkavacheMessageQueue"/>.
        /// </summary>
        /// <param name="akavacheCache"><see cref="AkavacheQueue"/>.</param>
        public AkavacheMessageQueue(AkavacheQueue akavacheCache)
        {
            _akavacheCache = akavacheCache;
        }

        /// <summary>
        /// Gets or sets the action to perform when a message is in the queue.
        /// </summary>
        public Func<string, Task> Command { get; set; }

        public bool DeadLetterEnabled { get; set; }

        public int RetryAttempts { get; set; }

        public long RetryInterval { get; set; }

        public int PollInterval { get; set; }

        public async Task EnqueueAsync(string message) =>
            await _akavacheCache.EnqueueMainAsync(Payload.Create(message));

        public async Task PurgeDeadLetterAsync()
        {
            var deadLetter = await _akavacheCache.DequeueDeadLetterAsync();
            if (deadLetter == null)
                return;

            await this.PurgeDeadLetterAsync();
        }

        public async Task RequeueAsync()
        {
            var deadLetter = await _akavacheCache.DequeueDeadLetterAsync();
            if (deadLetter == null)
                return;

            var payload = Payload.Create(deadLetter.Message);
            await _akavacheCache.EnqueueMainAsync(payload);

            await this.RequeueAsync();
        }

        public Task<long> GetCountAsync() =>
            _akavacheCache.GetMainCountAsync();

        public Task<long> GetRetryCountAsync() =>
            _akavacheCache.GetRetryCountAsync();

        public Task<long> GetDeadLetterCountAsync() =>
            _akavacheCache.GetDeadLetterCountAsync();

        /// <summary>
        /// Starts background tasks to process queues.
        /// </summary>
        public void Start()
        {
            _payloadTimer = new Timer(this.PollInterval);
            _payloadTimer.Elapsed += (__, _) => this.ProcessPayloads();
            _payloadTimer.Start();

            _retryTimer = new Timer(this.PollInterval);
            _retryTimer.Elapsed += (__, _) => this.ProcessRetries();
            _retryTimer.Start();
        }

        private async void ProcessPayloads()
        {
            try
            {
                _payloadTimer.Stop();

                await ProcessPayload();

                async Task ProcessPayload()
                {
                    var payload = await _akavacheCache.DequeueMainAsync();

                    if (payload == null)
                        return;

                    try
                    {
                        await this.Command?.Invoke(payload.Message);
                    }
                    catch (Exception exception)
                    {
                        Log.Error(exception);

                        // When an exception is thrown from the command determine where to move the message.
                        payload.RetryAttempts++;

                        Log.Info($"Payload retry attempts: {payload.RetryAttempts}. Retry attempts allowed: {this.RetryAttempts}.");

                        var moveToRetry = this.RetryAttempts > 0 &&
                                          this.RetryAttempts >= payload.RetryAttempts;
                        var moveToDeadLetter = this.DeadLetterEnabled &&
                                              !moveToRetry;

                        var retry = Retry.Create(payload.Message, payload.RetryAttempts, DateTime.UtcNow.AddMilliseconds(this.RetryInterval));
                        var deadLetter = DeadLetter.Create(payload.Message);

                        if (moveToRetry)
                        {
                            Log.Info("Moving payload to retry queue.");

                            await _akavacheCache.EnqueueRetryAsync(retry);
                        }
                        else if (moveToDeadLetter)
                        {
                            Log.Info("Moving payload to dead letter queue.");

                            await _akavacheCache.EnqueueDeadLetterAsync(deadLetter);
                        }
                    }
                    finally
                    {
                        await ProcessPayload();
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }
            finally
            {
                _payloadTimer.Start();
            }
        }

        private async void ProcessRetries()
        {
            try
            {
                _retryTimer.Stop();

                await ProcessRetry();

                async Task ProcessRetry()
                {
                    var nextRetry = await _akavacheCache.PeekRetryAsync();
                    if (nextRetry == null ||
                        nextRetry.ExpiredDateTime.ToLocalTime() > DateTime.Now)
                        return;

                    var retry = await _akavacheCache.DequeueRetryAsync();
                    await _akavacheCache.EnqueueMainAsync(retry.ToPayload());

                    await ProcessRetry();
                }
            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }
            finally
            {
                _retryTimer.Start();
            }
        }
    }
}
