﻿using SkyWire.SkyQueue.Builders;
using SkyWire.SkyQueue.Services;

namespace SkyWire.SkyQueue
{
    /// <summary>
    /// Represents helper methods for sky queue.
    /// </summary>
    public static class SkyQueueBuilder
    {
        /// <summary>
        /// Creates a <see cref="IMessageQueue"/> via the <see cref="IMessageQueueBuilder"/>.
        /// </summary>
        /// <param name="name">The location to store the queues data store.</param>
        /// <returns><see cref="IMessageQueueBuilderStorage"/>.</returns>
        public static IMessageQueueBuilderStorage Create(string name) =>
            new MessageQueueBuilder()
                .Create(name);
    }
}
