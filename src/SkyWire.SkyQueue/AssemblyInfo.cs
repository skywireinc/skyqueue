﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("SkyWire.SkyQueue.UnitTests")]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config", Watch = true)]