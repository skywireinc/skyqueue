﻿using System;

namespace SkyWire.SkyQueue.Models
{
    public class Payload : IAggregateRoot
    {
        public string Message { get; set; }
        public int RetryAttempts { get; set; }

        public static Payload Create(string message) =>
            new Payload
            {
                Message = message,
            };
    }
}
