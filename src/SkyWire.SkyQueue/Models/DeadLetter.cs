﻿using System;

namespace SkyWire.SkyQueue.Models
{
    public class DeadLetter : IAggregateRoot
    {
        public DateTime CreatedDateTime { get; set; }
        public string Message { get; set; }

        public static DeadLetter Create(string message) =>
            new DeadLetter
            {
                Message = message,
                CreatedDateTime = DateTime.UtcNow,
            };
    }
}
