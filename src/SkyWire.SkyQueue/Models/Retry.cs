﻿using System;

namespace SkyWire.SkyQueue.Models
{
    public class Retry : Payload
    {
        public DateTime ExpiredDateTime { get; set; }

        public static Retry Create(string message, int retryAttempts, DateTime expiration) =>
            new Retry
            {
                ExpiredDateTime = expiration,
                Message = message,
                RetryAttempts = retryAttempts,
            };
    }
}
