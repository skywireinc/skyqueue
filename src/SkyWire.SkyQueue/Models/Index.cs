﻿namespace SkyWire.SkyQueue.Models
{
    public class Index : IAggregateRoot
    {
        public long Current { get; set; }
    }
}
