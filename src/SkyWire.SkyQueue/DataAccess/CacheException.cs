﻿using System;
using System.Runtime.Serialization;

namespace SkyWire.SkyQueue.DataAccess
{
    public class CacheException : Exception
    {
        public CacheException()
        {
        }

        public CacheException(string message)
            : base(message)
        {
        }

        public CacheException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected CacheException(SerializationInfo info, StreamingContext context) 
            : base(info, context)
        {
        }
    }
}
