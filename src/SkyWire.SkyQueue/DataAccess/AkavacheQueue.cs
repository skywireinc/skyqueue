﻿using Akavache;
using log4net;
using SkyWire.SkyQueue.Common;
using SkyWire.SkyQueue.Models;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SkyWire.SkyQueue.DataAccess
{
    internal class AkavacheQueue
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly SemaphoreSlim Locker = new SemaphoreSlim(1, 1);
        private const string SKY_QUEUE = "skyqueue";

        private static readonly IDictionary<StorageType, IBlobCache> StorageTypeMappings =
            new Dictionary<StorageType, IBlobCache>
            {
                [StorageType.InMemory] = BlobCache.InMemory,
                [StorageType.Local] = BlobCache.LocalMachine,
                [StorageType.Secure] = BlobCache.Secure,
                [StorageType.UserAccount] = BlobCache.UserAccount,
            };

        private readonly StorageType _storageType;

        public AkavacheQueue(string applicationName, StorageType storageType)
        {
            BlobCache.ApplicationName = applicationName;
            _storageType = storageType;
        }

        private IBlobCache CurrentBlobCache => StorageTypeMappings[_storageType];

        public async Task EnqueueAsync<TEntity>(string queueName, TEntity entity)
            where TEntity : IAggregateRoot
        {
            await Locker.WaitAsync();

            try
            {
                var indexKey = this.GetIndexLastKey(queueName);

                // Get and set the next queue position
                var index = await this.GetOrCreateLastIndex(queueName);
                index.Current = index.Current.IncrementAndWrap();

                await this.CurrentBlobCache.InsertAllObjects(
                    new Dictionary<string, object>
                    {
                        [indexKey] = index,
                        [this.GetKey(queueName, index.Current)] = entity,
                    });
            }
            finally
            {
                Locker.Release();
            }
        }

        public async Task<TEntity> DequeueAsync<TEntity>(string queueName)
            where TEntity : IAggregateRoot
        {
            await Locker.WaitAsync();

            try
            {
                var indexKey = this.GetIndexFirstKey(queueName);

                // Get and set the current queue position.
                var index = await this.CurrentBlobCache.GetOrCreateObject(
                    indexKey,
                    () => new Index());

                var entity = default(TEntity);
                var entityKey = default(string);
                var lastIndex = await this.GetOrCreateLastIndex(queueName);

                // Retrieve the entity next in queue.
                // There is a rare occurrence where the blob cache may be corrupted such as when the application crashes.
                // In these rare instances the entity may be removed from the cache but without the first index being properly updated.
                // This is addressed by incrementing the first index until an entity is found or it reaches the last index.
                var mismatchExists = false;
                do
                {
                    entityKey = this.GetKey(queueName, index.Current);

                    entity = await this.GetEntityAsync<TEntity>(entityKey);

                    if (entity != null || index.Current >= lastIndex.Current)
                        break;

                    index.Current = index.Current.IncrementAndWrap();

                    Log.Error($"Queue mismatch was detected, there is a good chance the blob cache was corrupted. Skipping to next entity at index {index.Current}.");

                    mismatchExists = true;
                }
                while (mismatchExists);

                // If a mismatch occurred this will update the index in the cache.
                if (mismatchExists)
                    await this.CurrentBlobCache.InsertObject(indexKey, index);

                if (entity == null)
                    return default(TEntity);

                // Increment the next index.
                index.Current = index.Current.IncrementAndWrap();

                // Write changes
                await this.CurrentBlobCache.InsertObject(indexKey, index);
                await this.CurrentBlobCache.InvalidateObject<TEntity>(entityKey);

                return entity;
            }
            finally
            {
                Locker.Release();
            }
        }

        public async Task<TEntity> PeekAsync<TEntity>(string queueName)
            where TEntity : IAggregateRoot
        {
            var indexKey = this.GetIndexFirstKey(queueName);

            // Get the current queue position.
            var index = await this.CurrentBlobCache.GetOrCreateObject(
                indexKey,
                () => new Index());
            var key = this.GetKey(queueName, index.Current);

            // Retrieve the entity next in queue
            return await this.GetEntityAsync<TEntity>(key);
        }

        public async Task<long> GetCountAsync(string queueName)
        {
            var indexFirstKey = this.GetIndexFirstKey(queueName);
            var indexLastKey = this.GetIndexLastKey(queueName);

            try
            {
                var startIndex = await this.GetEntityAsync<Index>(indexFirstKey);
                var endIndex = await this.GetEntityAsync<Index>(indexLastKey);

                return (startIndex?.Current ?? 0).CountFromIndex((endIndex?.Current ?? -1).IncrementAndWrap());
            }
            catch
            {
                return 0;
            }
        }

        private async Task<Index> GetOrCreateLastIndex(string queueName)
        {
            var indexKey = this.GetIndexLastKey(queueName);

            return await this.CurrentBlobCache.GetOrCreateObject(
                    indexKey,
                    () => new Index { Current = -1 });
        }

        private async Task<TEntity> GetEntityAsync<TEntity>(string key)
            where TEntity : IAggregateRoot
        {
            try
            {
                return await this.CurrentBlobCache.GetObject<TEntity>(key);
            }
            catch (KeyNotFoundException)
            {
                return default(TEntity);
            }
        }

        private string GetKey(string queueName, long index) =>
            $"{SKY_QUEUE}-{queueName}-{index}";

        private string GetIndexFirstKey(string queueName) =>
            $"{SKY_QUEUE}-{queueName}-first";

        private string GetIndexLastKey(string queueName) =>
            $"{SKY_QUEUE}-{queueName}-last";
    }
}
