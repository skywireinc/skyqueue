﻿namespace SkyWire.SkyQueue.DataAccess
{
    internal enum StorageType
    {
        None,
        InMemory,
        Local,
        Secure,
        UserAccount,
    }
}
