﻿using SkyWire.SkyQueue.DataAccess;
using SkyWire.SkyQueue.Models;
using System.Threading.Tasks;

namespace SkyWire.SkyQueue.Common
{
    internal static class Extensions
    {
        private const long MAX_RECORDS = long.MaxValue;
        private const string MAIN_QUEUE = "main";
        private const string RETRY_QUEUE = "retry";
        private const string DEAD_LETTER_QUEUE = "deadletter";

        public static Payload ToPayload(this Retry retry) =>
            new Payload
            {
                Message = retry.Message,
                RetryAttempts = retry.RetryAttempts,
            };

        public static long IncrementAndWrap(this long source)
        {
            var nextIncrement = source + 1;

            return nextIncrement.Equals(MAX_RECORDS)
                ? 0
                : nextIncrement;
        }

        public static long CountFromIndex(this long startIndex, long endIndex)
        {
            if (startIndex <= endIndex)
                return endIndex - startIndex;

            var overflow = MAX_RECORDS - startIndex;

            return overflow + endIndex;
        }

        public static Task<long> GetMainCountAsync(this AkavacheQueue akavacheQueue) =>
            akavacheQueue.GetCountAsync(MAIN_QUEUE);

        public static Task<long> GetRetryCountAsync(this AkavacheQueue akavacheQueue) =>
            akavacheQueue.GetCountAsync(RETRY_QUEUE);

        public static Task<long> GetDeadLetterCountAsync(this AkavacheQueue akavacheQueue) =>
            akavacheQueue.GetCountAsync(DEAD_LETTER_QUEUE);

        public static Task EnqueueMainAsync(this AkavacheQueue akavacheQueue, Payload payload) =>
            akavacheQueue.EnqueueAsync(MAIN_QUEUE, payload);

        public static Task<Payload> DequeueMainAsync(this AkavacheQueue akavacheQueue) =>
            akavacheQueue.DequeueAsync<Payload>(MAIN_QUEUE);

        public static Task EnqueueRetryAsync(this AkavacheQueue akavacheQueue, Retry retry) =>
            akavacheQueue.EnqueueAsync(RETRY_QUEUE, retry);

        public static Task<Retry> DequeueRetryAsync(this AkavacheQueue akavacheQueue) =>
            akavacheQueue.DequeueAsync<Retry>(RETRY_QUEUE);

        public static Task<Retry> PeekRetryAsync(this AkavacheQueue akavacheQueue) =>
            akavacheQueue.PeekAsync<Retry>(RETRY_QUEUE);

        public static Task EnqueueDeadLetterAsync(this AkavacheQueue akavacheQueue, DeadLetter deadLetter) =>
            akavacheQueue.EnqueueAsync(DEAD_LETTER_QUEUE, deadLetter);

        public static Task<DeadLetter> DequeueDeadLetterAsync(this AkavacheQueue akavacheQueue) =>
            akavacheQueue.DequeueAsync<DeadLetter>(DEAD_LETTER_QUEUE);
    }
}
