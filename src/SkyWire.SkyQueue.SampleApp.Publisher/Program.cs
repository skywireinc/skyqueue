﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SkyWire.SkyQueue.SampleApp.Publisher
{
    public class Program
    {
        static void Main(string[] args)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            Console.CancelKeyPress += (_, __) => cancellationTokenSource.Cancel();

            try
            {
                var messageQueue = SkyQueueBuilder
                    .Create("skyqueue")
                    .UsingLocalStorage()
                    .Build();

                while (!cancellationTokenSource.IsCancellationRequested)
                {
                    Console.WriteLine("Input a message to be queued:");
                    var message = Console.ReadLine();

                    messageQueue
                        .EnqueueAsync(message)
                        .Wait();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}

