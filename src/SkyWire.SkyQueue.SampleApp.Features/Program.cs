﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SkyWire.SkyQueue.SampleApp.Features
{
    class Program
    {
        private const string COUNT_KEY = "counts";
        private const string PURGE_KEY = "purge";
        private const string REQUEUE_KEY = "requeue";

        static void Main(string[] args)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            Console.CancelKeyPress += (_, __) => cancellationTokenSource.Cancel(false);
            Console.WriteLine("Press ctrl + c to exit.");

            var messageQueue = SkyQueueBuilder
                .Create("skyqueue")
                .UsingLocalStorage()
                .Build();

            while(!cancellationTokenSource.IsCancellationRequested)
            {
                var dictionary = new Dictionary<string, Func<Task>>
                {
                    [COUNT_KEY] = async () =>
                    {
                        Console.WriteLine($"Main Queue: {await messageQueue.GetCountAsync()}.");
                        Console.WriteLine($"Retry Queue: {await messageQueue.GetRetryCountAsync()}.");
                        Console.WriteLine($"Dead Letter Queue: {await messageQueue.GetDeadLetterCountAsync()}.");
                    },
                    [PURGE_KEY] = async () =>
                    {
                        Console.WriteLine($"Purging {await messageQueue.GetDeadLetterCountAsync()} messages.");
                        await messageQueue.PurgeDeadLetterAsync();
                        Console.WriteLine($"Dead Letter Queue: {await messageQueue.GetDeadLetterCountAsync()}.");
                    },
                    [REQUEUE_KEY] = async () =>
                    {
                        Console.WriteLine($"Requeueing {await messageQueue.GetDeadLetterCountAsync()} messages.");
                        await messageQueue.RequeueAsync();
                        Console.WriteLine($"Main Queue: {await messageQueue.GetCountAsync()}.");
                    },
                };

                Console.WriteLine($"[{COUNT_KEY}]: Get the number of messages in each queue.");
                Console.WriteLine($"[{PURGE_KEY}]: Remove all messages from the dead letter queue.");
                Console.WriteLine($"[{REQUEUE_KEY}]: Move all messages from the dead letter queue into the main queue.");

                var input = Console.ReadLine();

                if (dictionary.ContainsKey(input.ToLower()))
                    Task.WaitAll(dictionary[input]());
            }
        }
    }
}
