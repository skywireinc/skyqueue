﻿using Akavache;
using SkyWire.SkyQueue.Services;
using System;
using System.Threading.Tasks;
using Xunit;

namespace SkyWire.SkyQueue.UnitTests
{
    public class MessageQueueBuilderTests
    {
        [Fact]
        public void Create_Sets_BlobCache_ApplicationName()
        {
            var expectedValue = "skyqueue";

            SkyQueueBuilder
                .Create(expectedValue)
                .UsingInMemoryStorage()
                .Build();

            var actualValue = BlobCache.ApplicationName;

            Assert.Equal(expectedValue, actualValue);
        }

        [Fact]
        public void WithRetry_Sets_RetryInterval_And_RetryAttempts()
        {
            var expectedRetryInterval = 1000;
            var expectedRetryAttempts = 1;

            var messageQueue = (AkavacheMessageQueue)SkyQueueBuilder
                .Create("skyqueue")
                .UsingInMemoryStorage()
                .Subscribe(m => Task.CompletedTask)
                .WithRetry(expectedRetryAttempts, expectedRetryInterval)
                .Build();

            var actualRetryInterval = messageQueue.RetryInterval;
            var actualRetryAttempts = messageQueue.RetryAttempts;

            Assert.Equal(expectedRetryInterval, actualRetryInterval);
            Assert.Equal(expectedRetryAttempts, actualRetryAttempts);
        }

        [Fact]
        public void Not_Using_WithDeadLetter_Sets_DeadLetterEnabled_To_False()
        {
            var messageQueue = (AkavacheMessageQueue)SkyQueueBuilder
                .Create("skyqueue")
                .UsingInMemoryStorage()
                .Subscribe(m => Task.CompletedTask)
                .Build();

            var actualDeadLetterInterval = messageQueue.DeadLetterEnabled;

            Assert.False(actualDeadLetterInterval);
        }

        [Fact]
        public void Subscribe_Sets_Command()
        {
            var expectedCommand = new Func<string, Task>(m => Task.CompletedTask);

            var messageQueue = (AkavacheMessageQueue)SkyQueueBuilder
                .Create("skyqueue")
                .UsingInMemoryStorage()
                .Subscribe(expectedCommand)
                .Build();

            var actualCommand = messageQueue.Command;

            Assert.Equal(expectedCommand, actualCommand);
        }

        [Fact]
        public void PollFor_Sets_PollInterval()
        {
            var expectedPollInterval = 1000;

            var messageQueue = (AkavacheMessageQueue)SkyQueueBuilder
                .Create("skyqueue")
                .UsingInMemoryStorage()
                .PollFor(expectedPollInterval)
                .Build();

            var actualPollInterval = messageQueue.PollInterval;

            Assert.Equal(expectedPollInterval, actualPollInterval);
        }

        [Fact]
        public void Create_Throws_NullReferenceException_When_Name_IsEmpty()
        {
            var action = new Action(() => SkyQueueBuilder.Create(string.Empty));

            Assert.Throws<NullReferenceException>(action);
        }

        [Fact]
        public void PollFor_Throws_ArgumentOutOfRangeException_When_Interval_Is_Less_Than_Zero()
        {
            var action = new Action(() => SkyQueueBuilder
                .Create("skyqueue")
                .UsingInMemoryStorage()
                .PollFor(-1));

            Assert.Throws<ArgumentOutOfRangeException>(action);
        }

        [Fact]
        public void WithRetry_Throws_ArgumentOutOfRangeException_When_Interval_Is_Less_Than_Zero()
        {
            var action = new Action(() => SkyQueueBuilder
                .Create("skyqueue")
                .UsingInMemoryStorage()
                .Subscribe(m => Task.CompletedTask)
                .WithRetry(1, -1));

            Assert.Throws<ArgumentOutOfRangeException>(action);
        }

        [Fact]
        public void WithRetry_Throws_ArgumentOutOfRangeException_When_RetryAttempts_Is_Less_Than_Zero()
        {
            var action = new Action(() => SkyQueueBuilder
                .Create("skyqueue")
                .UsingInMemoryStorage()
                .Subscribe(m => Task.CompletedTask)
                .WithRetry(-1, 1000));

            Assert.Throws<ArgumentOutOfRangeException>(action);
        }
    }
}
